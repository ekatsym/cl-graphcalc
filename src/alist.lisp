(in-package :graph)

(defun alist? (thing)
  (and (listp thing)
       (every #'consp thing)))

(deftype alist ()
  '(satisfies alist?))

(defun alist-cons (key val alist)
  (cons (cons key val) alist))

(defun alist (&rest args)
  (declare (optimize (speed 3)))
  (labels ((rec (lst acc)
             (cond ((null lst) acc)
                   ((null (cdr lst))
                    (cons (cons (car lst) nil)
                          acc))
                   (t (rec (cddr lst)
                           (alist-cons (first lst)
                                       (second lst)
                                       acc))))))
    (nreverse (rec args nil))))

(defun alist-val (key alist &key (test #'eql))
  (declare (optimize (speed 3))
           (type alist alist)
           (type function test))
  (labels ((rec (alst)
             (if (null alst)
                 nil
                 (destructuring-bind (head &rest tail) alst
                   (cons-bind head-key head-val head
                     (if (funcall test key head-key)
                         head-val
                         (rec tail)))))))
    (rec alist)))

(defun alist-all-vals (key alist &key (test #'eql))
  (declare (optimize (speed 3))
           (type alist alist)
           (type function test))
  (labels ((rec (alst acc)
             (if (null alst)
                 acc
                 (destructuring-bind (head &rest tail) alst
                   (cons-bind head-key head-val head
                     (if (funcall test key head-key)
                         (rec tail (cons head-val acc))
                         (rec tail acc)))))))
    (nreverse (rec alist nil))))

(defun alist-key (val alist &key (test #'eql))
  (declare (optimize (speed 3))
           (type alist alist)
           (type function test))
  (labels ((rec (alst)
             (if (null alst)
                 nil
                 (destructuring-bind (head &rest tail) alst
                   (cons-bind head-key head-val head
                     (if (funcall test val head-val)
                         head-key
                         (rec tail)))))))
    (rec alist)))

(defun alist-all-keys (val alist &key (test #'eql))
  (declare (optimize (speed 3))
           (type alist alist)
           (type function test))
  (labels ((rec (alst acc)
             (if (null alst)
                 acc
                 (destructuring-bind (head &rest tail) alst
                   (cons-bind head-key head-val head
                     (if (funcall test val head-val)
                         (rec tail (cons head-key acc))
                         (rec tail acc)))))))
    (nreverse (rec alist nil))))

(defun alist-key-val-swap (alist)
  (declare (optimize (speed 3))
           (type alist alist))
  (labels ((rec (alst acc)
             (declare (type alist alst)
                      (type list acc))
             (if (null alst)
                 acc
                 (destructuring-bind (head &rest tail) alst
                   (cons-bind key val head
                     (rec tail (alist-cons val key acc)))))))
    (nreverse (rec alist nil))))

(defun edges-alist->hash-table (alist &key (test #'eql))
  (declare (optimize (speed 3))
           (type alist alist))
  (let ((ht (make-hash-table :test test)))
    (labels ((rec (alst)
               (declare (type alist alst))
               (if (null alst)
                   ht
                   (destructuring-bind (head &rest tail) alst
                     (cons-bind key val head
                       (push val (gethash key ht))
                       (rec tail))))))
      (rec alist))))
