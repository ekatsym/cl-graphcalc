(in-package :graph)

(defmacro with-gensyms (symbols &body body)
  `(let ,(mapcar (lambda (sym) `(,sym (gensym ,(string sym)))) symbols)
     ,@body))

(defun symbolicate (&rest things)
  (values
    (intern
      (apply #'concatenate
             (cons 'string
                   (mapcar #'string things))))))

(defun butmember (item list &key (test #'eql))
  (labels ((rec (lst acc)
             (if (null lst)
                 list
                 (destructuring-bind (head &rest tail) lst
                   (if (funcall test item head)
                       (nreverse acc)
                       (rec tail (cons head acc)))))))
    (rec list nil)))

(defmacro define-class (name direct-superclasses &body body)
  `(defclass ,name (,@direct-superclasses)
     ,(mapcar (lambda (slot)
                (let ((slot (if (listp slot) slot (list slot))))
                  (destructuring-bind (slot-name &rest slot-options) slot
                    `(,slot-name :accessor ,(symbolicate name '- slot-name)
                                 :initarg ,(intern (string slot-name) :keyword)
                                 ,@slot-options))))
              (butmember :options body))
     ,@(cdr (member :options body))))

(defun filter (function list)
  (declare (optimize (speed 3))
           (type function function)
           (type list list))
  (labels ((rec (lst acc)
             (if (null lst)
                 acc
                 (destructuring-bind (head &rest tail) lst
                   (let ((result (funcall function head)))
                     (if result
                         (rec tail (cons result acc))
                         (rec tail acc)))))))
    (rec list nil)))

(defmacro cons-bind (car cdr expression &body body)
  `(let ((,car (car ,expression))
         (,cdr (cdr ,expression)))
     ,@body))
