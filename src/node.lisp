(in-package :graph)

(define-class node ()
  (function :type function)
  (input-number :type fixnum)
  (outputs :type vector))

(defun make-node (function input-number &key (time-lim 1000000))
  (make-instance 'node
                 :function function
                 :input-number input-number
                 :outputs (make-array time-lim :initial-element nil)))

(define-condition node-input-number-mismatch (simple-error)
  ((node :initarg :node :type node :reader node-input-number-mismatch-node)
   (inputs :initarg :inputs :reader node-input-number-mismatch-inputs))
  (:report (lambda (condition stream)
             (format stream "input-number of node is ~a, not ~a (from ~{~a~^, ~})."
                     (node-input-number
                       (node-input-number-mismatch-node condition))
                     (length
                       (node-input-number-mismatch-inputs condition))
                     (node-input-number-mismatch-inputs condition)))))

(defun node-run (node node-time &rest inputs)
  (declare (type node node))
  (if (= (node-input-number node) (length inputs))
      (let ((fn (node-function node))
            (outs (node-outputs node)))
        (setf (svref outs node-time)
              (apply fn inputs)))
      (error 'node-input-number-mismatch :node node :inputs inputs)))

(defun node-output (node node-time)
  (svref (node-outputs node) node-time))

