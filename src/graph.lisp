(in-package :graph)

(defun node-list? (object)
  (and (listp object)
       (every (lambda (x) (typep x 'node))
              object)))

(deftype node-list ()
  '(satisfies node-list?))

(define-class graph ()
  (nodes :type node-list)
  (edges :type hash-table)
  (redges :type hash-table))

(defun edges-alist->edges+sources-hash-table (edges-alist &key (test #'eql))
  (declare (type alist edges-alist))
  (values
    (edges-alist->hash-table edges-alist :test test)
    (edges-alist->hash-table (alist-key-val-swap edges-alist) :test test)))

(defun make-graph (nodes edges)
  (declare (type list nodes)
           (type alist edges))
  (multiple-value-bind (edges-table srcs-table) (edges-alist->edges+sources-hash-table edges)
    (make-instance 'graph
                   :nodes nodes
                   :edges edges-table
                   :redges srcs-table)))

(defmacro make-graph-with-bindings (bindings &body edges)
  `(let ,bindings
     (make-graph
       (list ,@(mapcar #'car bindings))
       (list ,@(mapcar (lambda (edge)
                         (destructuring-bind (key val) edge
                           `(cons ,key ,val)))
                       edges)))))














































;(defun graph-node-source-outputs (graph node graph-time)
;  (declare (type graph graph)
;           (type fixnum graph-time))
;  (mapcar (lambda (src)
;            (declare (type node src))
;            (node-output src graph-time))
;          (gethash node (graph-sources graph))))
;
;(defun graph-node-run (node node-time graph)
;  (let ((src-outs (mapcar (lambda (src) (svref (node-outputs src) (1- node-time)))
;                          (gethash node (graph-sources graph))))
;        (fn (node-function node))
;        (outs (graph-outputs graph)))
;    (when (every #'identity src-outs)
;      (setf (svref outs node-time)
;            (apply fn src-outs)))))
;
;(let ((i (make-node (lambda () 10) 0))
;      (a (make-node (lambda (i) (+ i i)) 1))
;      (b (make-node (lambda (a) (* a a)) 1))
;      (c (make-node (lambda (a b) (+ a b)) 2))
;      (o (make-node #'values 1)))
;  (graph-node-run i 0
;                  (make-graph
;                    (list i a b c o)
;                    (alist i a
;                           a b
;                           a c
;                           b c
;                           c o)))
;  )
;
;(make-graph-with-bindings
;  ((i (make-node (lambda () 10) 0))
;   (a (make-node (lambda (i) (+ i i)) 1))
;   (b (make-node (lambda (a) (* a a)) 1))
;   (c (make-node (lambda (a b) (+ a b)) 2))
;   (o (make-node #'values 1)))
;  (i a)
;  (a b)
;  (a c)
;  (b c)
;  (c o))
;
;(defun take-node (node-symbol graph)
;  (alist-val node-symbol (graph-nodes graph)))
;
;(defun take-node-output (node-time node)
;  (let ((time-diff (- (node-time node) node-time)))
;    (when (>= time-diff 0)
;      (nth time-diff (node-outputs node)))))
;
;(defun src-node-symbols (node-symbol graph)
;  (alist-all-keys node-symbol (graph-edges graph)))
;
;(defun graph-node-run (node-symbol node-time graph)
;  (declare (type symbol node-symbol)
;           (type fixnum node-time)
;           (type graph graph))
;  (let ((node (take-node node-symbol graph))
;        (src-node-outputs (mapcar (lambda (sym)
;                                    (take-node-output (1- node-time)
;                                                      (take-node sym graph)))
;                                  (src-node-symbols node-symbol graph))))
;    (when (every #'identity src-node-outputs)
;      (apply #'node-run
;             (cons node
;                   src-node-outputs)))))
;
;(defun graph-outputs (graph)
;  (declare (type graph graph))
;  (mapcar (lambda (sym-nd) (cons (car sym-nd) (node-outputs (cdr sym-nd))))
;          (graph-nodes graph)))
;
;(defun graph-run (graph)
;  (declare (optimize (speed 3))
;           (type graph graph))
;  (let ((symbols (mapcar #'car (graph-nodes graph)))
;        (graph-time (1+ (apply #'max (mapcar (lambda (sym-nd) (node-time (cdr sym-nd)))
;                                             (graph-nodes graph))))))
;    (labels ((rec (syms)
;               (when syms
;                 (mapc (lambda (sym) (node-in-graph-run sym graph-time graph))
;                       syms)
;                 (rec (remove-if (lambda (sym) (>= (node-time (take-node sym graph)) graph-time))
;                                 syms)))))
;      (rec symbols)
;      (graph-outputs graph))))
;
