(declaim (optimize (debug 3)))
(in-package :common-lisp)

(defpackage cl-graphcalc
  (:nicknames :graph)
  (:use :common-lisp)
  (:import-from :alexandria
                )
  (:export #:node
           #:node-new
           #:node-input-number
           #:node-function
           #:node-outputs
           #:node-time
           #:node-run
           #:graph
           #:graph-new
           #:take-node
           #:take-node-output
           )
  )
