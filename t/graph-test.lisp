(in-package :graph)

(defparameter *graph1*
  (graph-new
    ((i (node-new (lambda () 10) 0))
     (a (node-new (lambda (i) (* 2 i)) 1))
     (b (node-new (lambda (a) (* a a)) 1))
     (c (node-new (lambda (a b) (+ a b)) 2))
     (o (node-new (lambda (c) c) 1)))
    ((i . a)
     (a . b)
     (a . c)
     (b . c)
     (c . o))))

(time (dotimes (i 1000) (graph-run *graph1*)))

(defparameter *graph2*
  (graph-new
    ((i (node-new (lambda () 10) 0 :init-output 0))
     (a (node-new (lambda (i a) (* 2 i a)) 2 :init-output 0))
     (b (node-new (lambda (a b) (* a a b b)) 2 :init-output 0))
     (c (node-new (lambda (b c) (+ b c)) 2 :init-output 0))
     (o (node-new (lambda (c) c) 1)))
    ((i . a)
     (a . a)
     (a . b)
     (b . b)
     (b . c)
     (c . c)
     (c . o))))

(graph-run *graph2*)
(graph-outputs *graph2*)

