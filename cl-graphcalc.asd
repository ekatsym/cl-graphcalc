(defsystem cl-graphcalc
  :version "0.0.1"
  :author "Hirotetsu Hongo"
  :depends-on (alexandria)
  :components ((:module "src"
                :serial t
                :components
                ((:file "package")
                 (:file "util" :depends-on ("package"))
                 (:file "node" :depends-on ("package" "util"))
                 (:file "alist" :depends-on ("package" "util"))
                 (:file "graph" :depends-on ("package" "util" "node" "alist"))))))
